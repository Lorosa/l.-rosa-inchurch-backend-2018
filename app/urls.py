from rest_framework import routers
from django.urls import path, include
from . import views
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token




urlpatterns = [
    path('login/', obtain_jwt_token),
    path('token-refresh/', refresh_jwt_token),
    path('user_info/<int:pk>', views.users_info.as_view()),
    path('user_list', views.profile_list.as_view()),
    path('create_user', views.create_user.as_view()),
    path('update_userr/<int:pk>', views.update_userr.as_view()),
    path('delete_user/<int:pk>', views.delete_user.as_view()),
    path('change_password', views.change_password.as_view()),
    path('create_department', views.create_department.as_view()),
    path('delete_department/<int:pk>', views.delete_department.as_view()),
    path('avaliable_departaments', views.avl_departments.as_view()),
]