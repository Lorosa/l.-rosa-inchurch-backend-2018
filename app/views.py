from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import IsAdminUser
from django.contrib.auth.models import User
from rest_framework.response import Response
from .models import UserInfo
from .models import Department
from .serializers import UserInfoSerializer
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.permissions import AllowAny
from django.contrib.auth import authenticate



def user_full_profile(pk):
	user_full_info = {}

	user = User.objects.filter(id=pk).first()
	user_full_info['username'] =            user.username
	user_full_info['first_name'] =          user.first_name
	user_full_info['last_name'] =           user.last_name
	user_full_info['full_name'] =           user.userinfo.full_name
	user_full_info['email'] =               user.email
	try:
		user_full_info['department'] =     user.userinfo.department.name
	except:
		user_full_info['department'] =     ''
	user_full_info['password'] =            user.password
	user_full_info['groups'] =              user.groups.all()
	user_full_info['user_permissions'] =    user.user_permissions.all()
	user_full_info['is_staff'] =            user.is_staff
	user_full_info['is_active'] =           user.is_active
	user_full_info['is_superuser'] =        user.is_superuser
	user_full_info['last_login'] =          user.last_login
	user_full_info['date_joined'] =         user.date_joined

	return user_full_info


def user_slim_profile(user):
	""" 
					
	Function to not depends on serializers will anymore.

	"""
	user_info_list = {}
	user_info = {}
	user_info['uid'] = user.id
	user_info['full_name'] = UserInfo.objects.filter(user=user).first().full_name
	user_info_list['user'] = user_info

	return user_info_list

def users_slim_profile(user_object):
	"""

	this function was created to do the same job
	as serializers, but better.

	"""
	user_info_list = {}
	for user in user_object:
		user_info = {}
		user_info['uid'] = user.id
		user_info['full_name'] = UserInfo.objects.get(user=user).full_name
		
		user_info_list['user{}'.format(user.id)] = user_info
	
	return user_info_list


class users_details(viewsets.ModelViewSet):
	
	permisssion_classes = (IsAuthenticated,)

	def get(self, request, pk):
		return Response(user_full_profile(pk))


class users_info(APIView):

	permission_classes = (IsAuthenticated,)

	def get(self, request, pk):
		print(request.user.id)
		if request.user.id == pk or request.user.is_superuser:
			try :
				return Response(user_full_profile(pk))
			except:
				return Response({"Error ":"User not found"})
		else:
			return Response({'Not authorized':'Access Denied'})

	def put(self, request, pk):
		pass
		

class profile_list(APIView):

	permission_classes = (IsAuthenticated,)

	def get(self, request):
		user_list = User.objects.all()
		return Response(users_slim_profile(user_list))

class create_department(APIView):

	permission_classes = (IsAdminUser,)

	def post(self, request):
		dpt_name = request.POST.get('department_name', None)

		check_dpt_name = Department.objects.filter(name=dpt_name).exists()
		if dpt_name != None:
			if not check_dpt_name:
				try:
					dpt = Department.objects.create(name=dpt_name)
					return Response({'Created':'{} was created !'.format(dpt_name)})
				except Exception as ex:
					return Response({'Error':'Error creating department. Details: {}'.format(ex)})
			else:
				return Response({"Error":"Department already exists."})  
		else:
			return Response({"Error": "Fiel 'department_name' can't be none."})

class delete_department(APIView):

	permission_classes = (IsAdminUser,)

	def get(self, request, pk):
		dpt = Department.objects.filter(pk=pk)

		if dpt.exists():
			dpt_name = dpt.first().name
			try:
				dpt.first().delete()
				return Response({'Success':'Department {} was deleted !'.format(dpt_name)})
			except Exception as ex:
				return Response({'Error':'Error deleting objecs, try again. Details: {}'.format(ex)})
		else:
			return Response({'Error':'Department does not exists.'})

class avl_departments(APIView):
	"""
	Return all avaliable departments.

	"""
	def get(self, request):
		dps = Department.objects.all()
		return Response(list(map(lambda x: {x.id : x.name}, dps)))

class create_user(APIView):

	permission_classes = (AllowAny,)


	def post(self, request, format=None):
		username =      request.POST.get('username', 'none_username')
		email =         request.POST.get('email', 'none_email')
		password =      request.POST.get('password', 'none_password')
		full_name =     request.POST.get('full_name', 'none_full_name')
		department =   request.POST.get('department', 'none_departament')

		required_data = [username , email, password, full_name, department]
		left_items = []

		for item in required_data:
			if 'none_' in item:
				left_items.append(item)

		if len(left_items) > 0:
			missing_fields = {}
			loop = 0
			for field in left_items:
				loop += 1
				missing_fields['{}_Missing'.format(loop)] = field
			return Response(missing_fields)

		if len(left_items) == 0:
			user_exits = User.objects.filter(username=username).exists()
			
			if user_exits:
				return Response({'Error':'User already exists.'})
			
			else:
				try:
					new_user = User.objects.create_user(username=username,
														email=email,
														password=password,
														)
				except Exception as ex:
					return Response({'Error':'Failed to create user. Details: {}'.format(ex)})
				
				try:
					new_user.userinfo.full_name = full_name
					new_user.userinfo.department = Department.objects.filter(name=department).first()
					new_user.save()
				except Exception as ex:
					return Response({'Error':'Failed to create user extended info. Details: {}'.format(ex)})

				else:
					return Response(user_slim_profile(new_user))
		else:
			return Response({'Missing fields':'Please fill all required fields.'})


class update_userr(APIView):

	permission_classes = (IsAuthenticated,)

	def post(self, request, pk):
		try:
			edit = User.objects.get(pk=pk)
		except:
			return Response({"Error":"Invalid user"})
			
		full_name = request.POST.get('full_name', edit.userinfo.full_name)
		email = request.POST.get('email', edit.email)
		try:
			department = request.POST.get('department', edit.userinfo.department.id)
		except:
			department = None
		
		edit.userinfo.full_name = full_name
		edit.email = email
		try:
			edit.userinfo.department = Department.objects.get(pk=department)
		except:
			edit.userinfo.department = None
		
		if edit.id == request.user.id or request.user.is_superuser:
			if edit.userinfo.department.id == request.user.userinfo.department.id:
				try:
					edit.save()
					return Response({"Success":"User {} was edited !".format(edit.username)})
				except:
					return Response({"Error": "Error when saving changes."})
			else:
				return Response({"Not Authorized"})
		else:
			return Response({"Not Authorized"})
			
		
				
class delete_user(APIView):

	#permission_classes = (IsAuthenticated,)

	def get(self, request, pk):
		user_to_be_deleted = User.objects.filter(id=pk).first()
		if request.user != user_to_be_deleted or not request.user.is_superuser:
			if user_to_be_deleted:
				try:
					user_to_be_deleted.delete()
					return Response({'Deleted':'User {} was deleted.'.format(user_to_be_deleted.username)})
				except:
					return Response({'Error': 'An error have occurred when deleting user. Please try again later.'})
			else:
				return Response({'Error':'User does not exists'})
		else:
			return Response({'Not Allowed':'You are not allowed to perform this action.'})

class change_password(APIView):

	def get(self, request):
		old_password = request.POST.get('password', None)
		new_password = request.POST.get('new_password', None)

		if old_password and new_password != None:
			if authenticate(username=request.user.username, password=old_password):
				user = request.user
				try:
					user.set_password(new_password)
					user.save()
					return Response({"Success":"Your new password was set !"})
				except Exception as ex:	
					return Response({"Error":"{}".format(ex)})
			else:
				return Response({"Error":"Check your credentials."})
		else:
			return Response({"Error":"password and new_password fields can't be none."})