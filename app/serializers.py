from rest_framework import serializers
from django.contrib.auth.models import User
from .models import UserInfo
#from .models import Departament 

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class UserInfoSerializer(serializers.ModelSerializer):
    user = UserSerializer(User, many=False, read_only=True)
    departament = UserSerializer(User, many=False, read_only=True)
    class Meta:
        model = UserInfo
        fields = ('user', 'full_name', 'departament')
