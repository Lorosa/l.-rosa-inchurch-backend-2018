from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Department(models.Model):
    name = models.CharField(max_length=45)

class UserInfo(models.Model):
    # Extra info about User model 
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(blank=True, null=True, max_length=45)
    department = models.ForeignKey(Department, related_name='department', null=True, blank=True, on_delete=models.CASCADE) 

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserInfo.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userinfo.save()
