### Setting up the application

* Install requirements in requirements.txt
* Run makemigrations and migrate
* Create your superuser 

### Using the application

* Create a departament at '/create_department'
* Check avaliable departments at '/avaliable_departments'
* Create a new user at '/create_user'
* Login at '/login/'
* See your user info at '/user_info/<YOUR_USER_ID>'
* See a list of all registered users at '/user_list'
* Update you profile info at '/update_user/<YOUR_USER_ID>'
* Change your user password at '/change_password'
* Delete a department at '/delete_department/<DEPARTMENT_ID>'

### Auth system
* JWT